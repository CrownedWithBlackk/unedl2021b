﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuPrincipal
{
    class Program
    {
        /*
         * Capturar los valores de una matriz cuadrada
         */
        static int[,] LlenarMatriz()
        {
            String msize = "";
            int arrsize = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("Ingrese el tamaño de la matriz cuadrada:");
                Console.WriteLine("Entre 2 y 9");
                // TODO Validar entrada, unicamente entre 2 y 9
                msize = Console.ReadLine();
                arrsize = Convert.ToInt32(msize);
            } while (arrsize < 2 || arrsize > 9);

            // arreglo 2D o Matriz
            int[,] myarray = new int[arrsize, arrsize];
            //TODO algoritmo recibir valores matriz 
            
            for(int i = 0; i < arrsize; i++)
            {
                for(int k = 0;k < arrsize; k++)
                {
                    Console.Write("Ingrese el valor de la matriz["+i+"]["+k+"]: ");
                    myarray[i, k] = Convert.ToInt32(Console.ReadLine());
                }
            }

            
            /*int[,] myarray = new int[5, 5] {
                {1,2,3,8,6},
                {2,3,4,7,9},
                {3,4,5,9,4},
                {6,5,8,9,4},
                {1,2,8,3,5}};
            */
            return myarray;
        }

        /*
         * Recorrido en una matriz cuadrada, forma de N
         * Ejemplo: 
         * 1 2 3 8
         * 2 3 4 7
         * 3 4 5 9
         * 6 5 8 9
         * resultado: 1,2,3,6,5,4,3,2,3,4,5,8,9,9,7,8
         * n x n
         */
        static void RecorridoMatriz1(int[,] myarray)
        {
            int sizearray = myarray.GetLength(0);
            Console.WriteLine();
            bool variable = true;
            // Mover en las columnas
            for(int i = 0; i < myarray.GetLength(0); i++)
            {
                if (variable) // pares
                {
                    for (int j = 0; j < myarray.GetLength(1); j++)
                    {
                        Console.Write(myarray[j, i] + " , ");
                        variable = false;
                    }
                }
                else         // nones
                {
                    for (int j = myarray.GetLength(1) - 1; j >= 0 ; j--)
                    {
                        Console.Write(myarray[j, i] + " , ");
                        variable = true;
                    }
                }
            }
            Console.ReadKey();        
        }

        /*
        * Recorrido en una matriz cuadrada, forma de N
        * Ejemplo: 
        * 1 2 3 8
        * 2 3 4 7
        * 3 4 5 9
        * 6 5 8 9
        * resultado: 1,2,3,6,5,4,3,2,3,4,5,8,9,9,7,8
        * n x n
        */
        static void RecorridoMatriz2(int[,] myarray)
        {
            int sizearray = myarray.GetLength(0);
            Console.WriteLine();
            // Mover en las columnas
            for (int i = 0; i < myarray.GetLength(0); i++)
            {
                for (int j = 0; j < myarray.GetLength(1); j++)
                {
                    if ((i % 2) == 0) // pares
                        Console.Write(myarray[j, i] + " ");
                    else          // nones
                        Console.Write(
                            myarray[myarray.GetLength(1) - j - 1, i] + " ");
                }
            }
            Console.ReadKey();
        }

        /*
         * Menu Principal
         * returns bool to quit
         */
        static bool MenuPrincipal()
        {
            bool respuesta = false;
            String opcion = "";
            Console.Clear();
            Console.WriteLine("###############");
            Console.WriteLine("Menu Principal");
            Console.WriteLine("###############");
            Console.WriteLine("");
            Console.WriteLine("Seleccione la opcion");
            Console.WriteLine("1) Recorrido Matriz 1");
            Console.WriteLine("2) Recorrido Matriz 2");
            Console.WriteLine("3) Diagonal Difference");
            Console.WriteLine("4) Bitonico");
            Console.WriteLine("0) Salir");
            opcion = Console.ReadLine();
            switch (opcion)
            {
                case "0":
                    respuesta = true;
                    break;
                case "1":
                    RecorridoMatriz1(LlenarMatriz());
                    break;
                case "2":
                    RecorridoMatriz2(LlenarMatriz());
                    break;
                case "3":
                    //DiagonalDifference();
                    break;
                case "4":
                    //Bitonico();
                    break;
            }
            return respuesta;
        }

        /*
         * Main program
         */
        static void Main(string[] args)
        {
            bool salir = false;

            while (!salir)
            {
                salir = MenuPrincipal();
            }
        }
    }
}
