        static int[,] LlenarMatriz()
        {
            String msize = "";
            int arrsize = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("Ingrese el tamaño de la matriz cuadrada:");
                Console.WriteLine("Entre 2 y 9");
                // TODO Validar entrada, unicamente entre 2 y 9
                msize = Console.ReadLine();
                arrsize = Convert.ToInt32(msize);
            } while (arrsize < 2 || arrsize > 9);

            // arreglo 2D o Matriz
            int[,] myarray = new int[arrsize, arrsize];
            //TODO algoritmo recibir valores matriz 
            
            for(int i = 0; i < arrsize; i++)
            {
                for(int k = 0;k < arrsize; k++)
                {
                    Console.Write("Ingrese el valor de la matriz["+i+"]["+k+"]: ");
                    myarray[i, k] = Convert.ToInt32(Console.ReadLine());
                }
            }

            return myarray;
        }