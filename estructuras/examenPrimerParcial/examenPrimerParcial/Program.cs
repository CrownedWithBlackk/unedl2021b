﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examenPrimerParcial
{
    class Program
    { 
        // Un palíndromo es una palabra o frase que se lee igual 
        // de izquierda a derecha que de derecha a izquierda.
        static bool isPalindromo(string word)
        {
            bool resultado = true;
            string wordLowC = word.ToLower();
            wordLowC = wordLowC.Replace(" ", "");

            if (wordLowC.Contains('á'))
                wordLowC=wordLowC.Replace('á', 'a');

            if (wordLowC.Contains('é'))
                wordLowC = wordLowC.Replace('é','e');

            if (wordLowC.Contains('í'))
                wordLowC = wordLowC.Replace('í', 'i');

            if (wordLowC.Contains('ó'))
                wordLowC = wordLowC.Replace('ó', 'o');

            if (wordLowC.Contains('ú'))
                wordLowC = wordLowC.Replace('ú', 'u');

            char[] charWordLowC = wordLowC.ToCharArray();
            char[] normalCharWord = wordLowC.ToCharArray();
            Array.Reverse(charWordLowC);
            string reversedWordLowC = new string(charWordLowC);
            char[] reversedCharWord = reversedWordLowC.ToCharArray();

           for(int i = 0; i < reversedCharWord.Length; i++)
           {
               if (normalCharWord[i]==(reversedCharWord[i]))
               {
               }
               else
               {
                   resultado = false;
               }
                
           }
            return resultado;
        }

        // Metodo principal
        static void Main(string[] args)
        {
            string[] sarray = { "Acaso hubo búhos acá", "A cavar a Caravaca",
                "Allí ves Sevilla", "Amad a la dama" };

            foreach (string word in sarray)
            {
                if (isPalindromo(word))
                {
                    Console.WriteLine("La palabra: {0} es un palíndromo", word);
                    
                }
                else
                {
                    Console.WriteLine("La palabra: {0} no es un palíndromo", word);
                }
            }
        }
    }
}
