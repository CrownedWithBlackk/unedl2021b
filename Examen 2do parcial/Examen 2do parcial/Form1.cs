﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Examen_2do_parcial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Examen 2do Parcial";
            comboBox1.Items.Insert(0,"Seleccione una opcion");
            comboBox1.Items.Insert(1,"FULL");
            comboBox1.Items.Insert(2,"PART");
            comboBox1.Items.Insert(3,"MIN");
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.Insert(0,"Seleccione una opcion");
            comboBox2.Items.Insert(1,"NORMAL");
            comboBox2.Items.Insert(2,"HIGH");
            comboBox2.Items.Insert(3,"LOW");
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.SelectedIndex = 0;


            try
            {
                string nombreArchivo = @"C:\Users\Ferna\Desktop\Escuela\Programacion3\c#\Examen 2do parcial\Examen 2do parcial\segundo_examen_parcial_parametros.txt";
                List<string> lineasArchivo = File.ReadAllLines(nombreArchivo).ToList();

                for (int i = 0; i < lineasArchivo.Count; i++)
                {
                    if (lineasArchivo[i].Contains("#"))
                    {

                    }
                    else
                    {
                        txtSalida.AppendText(lineasArchivo[i].ToString());
                        txtSalida.AppendText(Environment.NewLine);
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Hubo un problema con el PATH del archivo, verifique el fichero\nError 404, archivo no encontrado\nCerrando el programa...","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                this.Close();
            }





        }

        public void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtInstallPassword.Text == "")
                MessageBox.Show("El campo contraseña no puede quedar vacio");
            else
                try
                {
                    guardarDatos();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Hubo un problema con el PATH del archivo, verifique el fichero\nError 404, archivo no encontrado\nCerrando el programa...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
                
            if (txtLocalidadBase.Text =="")
                MessageBox.Show("El campo localidad base no puede quedar vacio");
                 
        }

        private void guardarDatos()
        {
            string nombreArchivo = @"C:\Users\Ferna\Desktop\Escuela\Programacion3\c#\Examen 2do parcial\Examen 2do parcial\segundo_examen_parcial_parametros.txt";
            List<string> lineasArchivo = File.ReadAllLines(nombreArchivo).ToList();

            string temp = lineasArchivo[8];
            lineasArchivo.Remove(temp);
            lineasArchivo.Insert(8, "LOCALIDADINVENTARIO=" + txtLocalidadInventario.Text.Trim());

            temp = lineasArchivo[19];
            lineasArchivo.Remove(temp);
            if (comboBox1.SelectedIndex.Equals(0))

                lineasArchivo.Insert(19, "install.option=FULL");
            else
                lineasArchivo.Insert(19, "install.option=" + comboBox1.Text);

            temp = lineasArchivo[25];
            lineasArchivo.Remove(temp);
            lineasArchivo.Insert(25, "LOCALIDADBASE=" + txtLocalidadBase.Text.Trim());

            temp = lineasArchivo[31];
            lineasArchivo.Remove(temp);
            lineasArchivo.Insert(31, "install.port=" + txtInstallPort.Text.Trim());

            temp = lineasArchivo[37];
            lineasArchivo.Remove(temp);
            lineasArchivo.Insert(37, "install.address=" + txtInstallAddres1.Text.Trim()+"."+txtInstallAddres2.Text.Trim()+"."+txtInstallAddres3.Text.Trim()+"."+txtInstallAddres4.Text.Trim());

            temp = lineasArchivo[43];
            lineasArchivo.Remove(temp);
            lineasArchivo.Insert(43, @"install.password=""" + txtInstallPassword.Text.Trim()+"\"");




            temp = lineasArchivo[54];
            lineasArchivo.Remove(temp); 
            if (comboBox2.SelectedIndex.Equals(0))
                lineasArchivo.Insert(54, "install.redundancia=NORMAL");
            else
                lineasArchivo.Insert(54, "install.redundancia=" + comboBox2.Text);

            File.WriteAllLines(nombreArchivo, lineasArchivo);



            txtSalida.Text = null;
            for (int i = 0; i < lineasArchivo.Count; i++)
            {
                if (lineasArchivo[i].Contains("#"))
                {

                }
                else
                {
                    txtSalida.AppendText(lineasArchivo[i].ToString());
                    txtSalida.AppendText(Environment.NewLine);
                }
            }
        }
            

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
