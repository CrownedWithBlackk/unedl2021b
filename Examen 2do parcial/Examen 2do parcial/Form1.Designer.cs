﻿
namespace Examen_2do_parcial
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtInstallAddres4 = new System.Windows.Forms.TextBox();
            this.txtInstallAddres2 = new System.Windows.Forms.TextBox();
            this.txtInstallAddres3 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtInstallPassword = new System.Windows.Forms.TextBox();
            this.txtInstallAddres1 = new System.Windows.Forms.TextBox();
            this.txtInstallPort = new System.Windows.Forms.TextBox();
            this.txtLocalidadBase = new System.Windows.Forms.TextBox();
            this.txtLocalidadInventario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSalida = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtSalida);
            this.panel1.Controls.Add(this.txtInstallAddres4);
            this.panel1.Controls.Add(this.txtInstallAddres2);
            this.panel1.Controls.Add(this.txtInstallAddres3);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.btnSalir);
            this.panel1.Controls.Add(this.btnGuardar);
            this.panel1.Controls.Add(this.txtInstallPassword);
            this.panel1.Controls.Add(this.txtInstallAddres1);
            this.panel1.Controls.Add(this.txtInstallPort);
            this.panel1.Controls.Add(this.txtLocalidadBase);
            this.panel1.Controls.Add(this.txtLocalidadInventario);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 379);
            this.panel1.TabIndex = 0;
            // 
            // txtInstallAddres4
            // 
            this.txtInstallAddres4.Location = new System.Drawing.Point(314, 182);
            this.txtInstallAddres4.Name = "txtInstallAddres4";
            this.txtInstallAddres4.Size = new System.Drawing.Size(26, 20);
            this.txtInstallAddres4.TabIndex = 8;
            // 
            // txtInstallAddres2
            // 
            this.txtInstallAddres2.Location = new System.Drawing.Point(231, 182);
            this.txtInstallAddres2.Name = "txtInstallAddres2";
            this.txtInstallAddres2.Size = new System.Drawing.Size(26, 20);
            this.txtInstallAddres2.TabIndex = 6;
            // 
            // txtInstallAddres3
            // 
            this.txtInstallAddres3.Location = new System.Drawing.Point(273, 182);
            this.txtInstallAddres3.Name = "txtInstallAddres3";
            this.txtInstallAddres3.Size = new System.Drawing.Size(26, 20);
            this.txtInstallAddres3.TabIndex = 7;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(194, 265);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(182, 21);
            this.comboBox2.TabIndex = 10;
            this.comboBox2.Text = "Seleccione una opcion";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(164, 349);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 19);
            this.button1.TabIndex = 17;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(193, 63);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(184, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Text = "Seleccione una opcion";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(231, 320);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(56, 19);
            this.btnSalir.TabIndex = 12;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(78, 320);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(56, 19);
            this.btnGuardar.TabIndex = 11;
            this.btnGuardar.Text = "Guardar Cambios";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtInstallPassword
            // 
            this.txtInstallPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstallPassword.Location = new System.Drawing.Point(193, 223);
            this.txtInstallPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtInstallPassword.Multiline = true;
            this.txtInstallPassword.Name = "txtInstallPassword";
            this.txtInstallPassword.PasswordChar = '*';
            this.txtInstallPassword.Size = new System.Drawing.Size(183, 23);
            this.txtInstallPassword.TabIndex = 9;
            // 
            // txtInstallAddres1
            // 
            this.txtInstallAddres1.Location = new System.Drawing.Point(194, 182);
            this.txtInstallAddres1.Margin = new System.Windows.Forms.Padding(2);
            this.txtInstallAddres1.Name = "txtInstallAddres1";
            this.txtInstallAddres1.Size = new System.Drawing.Size(26, 20);
            this.txtInstallAddres1.TabIndex = 5;
            // 
            // txtInstallPort
            // 
            this.txtInstallPort.Location = new System.Drawing.Point(193, 141);
            this.txtInstallPort.Margin = new System.Windows.Forms.Padding(2);
            this.txtInstallPort.Name = "txtInstallPort";
            this.txtInstallPort.Size = new System.Drawing.Size(183, 20);
            this.txtInstallPort.TabIndex = 4;
            // 
            // txtLocalidadBase
            // 
            this.txtLocalidadBase.Location = new System.Drawing.Point(194, 104);
            this.txtLocalidadBase.Margin = new System.Windows.Forms.Padding(2);
            this.txtLocalidadBase.Name = "txtLocalidadBase";
            this.txtLocalidadBase.Size = new System.Drawing.Size(183, 20);
            this.txtLocalidadBase.TabIndex = 3;
            // 
            // txtLocalidadInventario
            // 
            this.txtLocalidadInventario.Location = new System.Drawing.Point(194, 25);
            this.txtLocalidadInventario.Margin = new System.Windows.Forms.Padding(2);
            this.txtLocalidadInventario.Name = "txtLocalidadInventario";
            this.txtLocalidadInventario.Size = new System.Drawing.Size(183, 20);
            this.txtLocalidadInventario.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(63, 272);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Sistema de redundancia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 227);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Password de instalación";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 189);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Dirección IP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Puerto de comunicación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Localidad base";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Opciones de instalación";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Localidad del inventario";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(215, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 31);
            this.label8.TabIndex = 5;
            this.label8.Text = ".";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(254, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 31);
            this.label9.TabIndex = 19;
            this.label9.Text = ".";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(295, 174);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 31);
            this.label10.TabIndex = 20;
            this.label10.Text = ".";
            // 
            // txtSalida
            // 
            this.txtSalida.Location = new System.Drawing.Point(416, 25);
            this.txtSalida.Multiline = true;
            this.txtSalida.Name = "txtSalida";
            this.txtSalida.Size = new System.Drawing.Size(266, 299);
            this.txtSalida.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 379);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtInstallPassword;
        private System.Windows.Forms.TextBox txtInstallAddres1;
        private System.Windows.Forms.TextBox txtInstallPort;
        private System.Windows.Forms.TextBox txtLocalidadBase;
        private System.Windows.Forms.TextBox txtLocalidadInventario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox txtInstallAddres4;
        private System.Windows.Forms.TextBox txtInstallAddres2;
        private System.Windows.Forms.TextBox txtInstallAddres3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSalida;
    }
}

