/*
import javax.swing.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ReglaFalsaVentanaPrincipal extends JFrame{

    private JTextField txtIntervaloMayor;
    private JTextField txtNumeroDecimales;
    private JTextField txtPorcentajeError;
    private JTextField txtX6;
    private JTextField txtIntervaloMenor;
    private JTextField txtX5;
    private JTextField txtX4;
    private JTextField txtX3;
    private JTextField txtX2;
    private JTextField txtX;
    private JTextField txtConstante;
    private JButton btnCalcular;
    private JButton btnLimpiarSalida;
    private JButton btnLimpiarCampos;
    private JLabel lblIntervaloMenor;
    private JLabel lblIntervaloMayor;
    private JLabel lblNumeroDecimales;
    private JLabel lblPorcentajeError;
    private JLabel lblX6;
    private JLabel lblX5;
    private JLabel lblX4;
    private JLabel lblX3;
    private JLabel lblX2;
    private JLabel lblX;
    private JLabel lblConstante;
    private JPanel panelMain;
    private JTextArea textArea;

    public JTextField getTxtIntervaloMayor() {
        return txtIntervaloMayor;
    }

    public void setTxtIntervaloMayor(JTextField txtIntervaloMayor) {
        this.txtIntervaloMayor = txtIntervaloMayor;
    }

    public JTextField getTxtNumeroDecimales() {
        return txtNumeroDecimales;
    }

    public void setTxtNumeroDecimales(JTextField txtNumeroDecimales) {
        this.txtNumeroDecimales = txtNumeroDecimales;
    }

    public JTextField getTxtPorcentajeError() {
        return txtPorcentajeError;
    }

    public void setTxtPorcentajeError(JTextField txtPorcentajeError) {
        this.txtPorcentajeError = txtPorcentajeError;
    }

    public JTextField getTxtX6() {
        return txtX6;
    }

    public void setTxtX6(JTextField txtX6) {
        this.txtX6 = txtX6;
    }

    public JTextField getTxtIntervaloMenor() {
        return txtIntervaloMenor;
    }

    public void setTxtIntervaloMenor(JTextField txtIntervaloMenor) {
        this.txtIntervaloMenor = txtIntervaloMenor;
    }

    public JTextField getTxtX5() {
        return txtX5;
    }

    public void setTxtX5(JTextField txtX5) {
        this.txtX5 = txtX5;
    }

    public JTextField getTxtX4() {
        return txtX4;
    }

    public void setTxtX4(JTextField txtX4) {
        this.txtX4 = txtX4;
    }

    public JTextField getTxtX3() {
        return txtX3;
    }

    public void setTxtX3(JTextField txtX3) {
        this.txtX3 = txtX3;
    }

    public JTextField getTxtX2() {
        return txtX2;
    }

    public void setTxtX2(JTextField txtX2) {
        this.txtX2 = txtX2;
    }

    public JTextField getTxtX() {
        return txtX;
    }

    public void setTxtX(JTextField txtX) {
        this.txtX = txtX;
    }

    public JTextField getTxtConstante() {
        return txtConstante;
    }

    public void setTxtConstante(JTextField txtConstante) {
        this.txtConstante = txtConstante;
    }

    public ReglaFalsaVentanaPrincipal(String title){
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(panelMain);

        this.setSize(600,500);
        this.pack();

        btnCalcular.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    calcular();


            }
        });
        txtIntervaloMenor.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtIntervaloMenor.selectAll();
            }
        });
        txtIntervaloMayor.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtIntervaloMayor.selectAll();
            }
        });
        txtNumeroDecimales.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtNumeroDecimales.selectAll();
            }
        });
        txtPorcentajeError.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtPorcentajeError.selectAll();
            }
        });
        txtX6.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtX6.selectAll();
            }
        });
        txtX5.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtX5.selectAll();
            }
        });
        txtX4.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtX4.selectAll();
            }
        });
        txtX3.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                txtX3.selectAll();
            }
        });
        txtX2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtX2.selectAll();
            }
        });
        txtX.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtX.selectAll();
            }
        });
        txtConstante.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                txtConstante.selectAll();
            }
        });
        btnLimpiarSalida.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText(null);
            }
        });

        btnCalcular.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                ImageIcon amlo2 = new ImageIcon("C:\\Users\\Ferna\\Desktop\\Escuela\\metodosNumericos\\regla falsa\\src\\amloBtn.png");
                btnCalcular.setIcon(amlo2);
            }
        });
        btnCalcular.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                ImageIcon amlo = new ImageIcon("C:\\Users\\Ferna\\Desktop\\Escuela\\metodosNumericos\\regla falsa\\src\\amloBtnLaser.png");
                btnCalcular.setIcon(amlo);
            }
        });
    }

    public void calcular(){

        BigDecimal intervaloMenor = BigDecimal.valueOf(Float.parseFloat(txtIntervaloMenor.getText().trim()));
        BigDecimal intervaloMayor = BigDecimal.valueOf(Float.parseFloat(txtIntervaloMayor.getText().trim()));
        int numeroDecimales = Integer.parseInt(txtNumeroDecimales.getText().trim());
        BigDecimal porcentajeError = BigDecimal.valueOf(Float.parseFloat(txtPorcentajeError.getText().trim()));

        porcentajeError=porcentajeError.divide(BigDecimal.valueOf(1),numeroDecimales,RoundingMode.HALF_UP);
        intervaloMenor=intervaloMenor.divide(BigDecimal.valueOf(1),numeroDecimales,RoundingMode.HALF_UP);
        intervaloMayor=intervaloMayor.divide(BigDecimal.valueOf(1),numeroDecimales,RoundingMode.HALF_UP);

        BigDecimal X6 = BigDecimal.valueOf(Float.parseFloat(txtX6.getText().trim()));
        BigDecimal X5 = BigDecimal.valueOf(Float.parseFloat(txtX5.getText().trim()));
        BigDecimal X4 = BigDecimal.valueOf(Float.parseFloat(txtX4.getText().trim()));
        BigDecimal X3 = BigDecimal.valueOf(Float.parseFloat(txtX3.getText().trim()));
        BigDecimal X2 = BigDecimal.valueOf(Float.parseFloat(txtX2.getText().trim()));
        BigDecimal X = BigDecimal.valueOf(Float.parseFloat(txtX.getText().trim()));
        BigDecimal cons = BigDecimal.valueOf(Float.parseFloat(txtConstante.getText().trim()));



        MathContext mc = new MathContext(numeroDecimales);

        BigDecimal fX0, fX1;
        BigDecimal Xrn = new BigDecimal(0);
        BigDecimal fXr = new BigDecimal(0);
        BigDecimal error = new BigDecimal(0);

        //////////////////////////////////Calculos///////////////////////////////////

        fX0= X6.multiply(intervaloMenor.pow(6,mc),mc).add(X5.multiply(intervaloMenor.pow(5,mc),mc)).add(X4.multiply(intervaloMenor.pow(4,mc),mc)).add(X3.multiply(intervaloMenor.pow(3,mc),mc)).add(X2.multiply(intervaloMenor.pow(2,mc),mc)).add(X.multiply(intervaloMenor,mc)).add(cons);
        fX1= X6.multiply(intervaloMayor.pow(6,mc),mc).add(X5.multiply(intervaloMayor.pow(5,mc),mc)).add(X4.multiply(intervaloMayor.pow(4,mc),mc)).add(X3.multiply(intervaloMayor.pow(3,mc),mc)).add(X2.multiply(intervaloMayor.pow(2,mc),mc)).add(X.multiply(intervaloMayor,mc)).add(cons);

        if(fX0.multiply(fX1).compareTo(BigDecimal.ZERO)<0){
            textArea.append("FX0: "+fX0);
            fXr= ((fX0.multiply(intervaloMayor,mc)).subtract(fX1.multiply(intervaloMenor,mc),mc)).divide(fX0.subtract(fX1),numeroDecimales, RoundingMode.HALF_UP);
        }
        else{
            textArea.append("\nLa raíz no se encuentra en el intervalo");
        }

        textArea.append("fXr: "+fXr);
        error= ((fXr.subtract(intervaloMenor)).divide(fXr,numeroDecimales,RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(100),mc).abs();


        //////////////Segunda iteracion////////////////
        textArea.setText(null);

            ///Calculo de error////
        int cont=1;

        do{
            if(cont>1){
                if(cont==2)
                    intervaloMenor=fXr;
                if(cont>2)
                    intervaloMenor=Xrn;

                fX0= X6.multiply(intervaloMenor.pow(6,mc),mc).add(X5.multiply(intervaloMenor.pow(5,mc),mc)).add(X4.multiply(intervaloMenor.pow(4,mc),mc)).add(X3.multiply(intervaloMenor.pow(3,mc),mc)).add(X2.multiply(intervaloMenor.pow(2,mc),mc)).add(X.multiply(intervaloMenor,mc)).add(cons);
                Xrn=(fX1.multiply(intervaloMenor,mc).subtract(fX1.multiply(intervaloMayor,mc))).divide(fX0.subtract(fX1),numeroDecimales,RoundingMode.HALF_UP);
                Xrn = BigDecimal.valueOf(1).subtract(Xrn);
                error= ((Xrn.subtract(intervaloMenor)).divide(Xrn,numeroDecimales,RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(100),mc).abs();
            }

            textArea.append("Error usuario: "+porcentajeError);
            textArea.append("\n("+(cont)+") El valor de Fx0 es: "+fX0);
            textArea.append("\nEl valor del intervalo menor es: "+intervaloMenor);
            textArea.append("\nEl valor de Fx1 es: "+fX1);
            textArea.append("\nEl valor del intervalo mayor es: "+intervaloMayor);
            textArea.append("\nEl valor de Fxr es: "+fXr);
            textArea.append("\nEl valor de Xrn es: "+Xrn);
            textArea.append("\nEl porcentaje de error es: "+error+"%\n\n");

            cont++;
        }while (porcentajeError.compareTo(error)<0);


    }

    public static void main(String[] args) {
        JFrame frame = new ReglaFalsaVentanaPrincipal("Regla Falsa");
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        JTextArea areaTexto = new JTextArea("prueba");
        JScrollPane scrollPane = new JScrollPane(areaTexto);
        scrollPane.setBounds(10,60,780,500);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        areaTexto.setEditable(false);

        ImageIcon icon = new ImageIcon("C:\\Users\\Ferna\\Desktop\\Escuela\\metodosNumericos\\regla falsa\\src\\amlo2.png");
        frame.setIconImage(icon.getImage());


    }

}





*/