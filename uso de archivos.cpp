#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <string>

using namespace std;

string crearArchivo();
void agregarRegistro(string nombreArchivo);
void leerRegistro(string nombreArchivo);
void borrarContenido(string nombreArchivo);

ofstream archivoOf;
ifstream archivoIf;
int main() {
    bool archivoCreado = false;
    string nombreArchivo;
    string op;
    do {
        cout << "\na) Crear archivo";
        cout << "\nb) Agregar un registro al archivo";
        cout << "\nc) Leer registros del archivo";
        cout << "\nd) Borrar contenido del archivo";
        cout << "\ne) Salir del programa";
        cout << "\nSeleccione una opcion: ";
        cin >> op;

        if (op == "a") {
            nombreArchivo=crearArchivo();
        }
        else if (op == "b")
                agregarRegistro(nombreArchivo);
        else if (op == "c")
            leerRegistro(nombreArchivo);
        else if (op == "d")
            borrarContenido(nombreArchivo);
        else if (op == "e")
            cout << "\nSaliendo...\n";
        else
            cout << "\nError, opcion invalida";
    } while (op != "e");
    system("pause");
    return 0;
}

string crearArchivo() {
    string nombreArchivo;
    cout << "creando archivo...";
    cout << "\nIngrese el nombre del archivo a crear: ";
    cin >> nombreArchivo;
    archivoIf.open(nombreArchivo.c_str(),ios::out);
    archivoIf.close();
    return nombreArchivo+".txt";
}

void agregarRegistro(string nombreArchivo) {
    string textoArchivo,nombre;
    cout << "agregando registro...";
    cout << "\nIngrese el nombre del archivo: ";
    cin >> nombre;
    nombre = nombre + ".txt";
    if (nombre == nombreArchivo) {
        archivoOf.open(nombreArchivo.c_str(), ios::app);
        if (archivoOf.fail()) {
            cout << "\nno se puede abrir el archivo";
            exit(1);
        }
        cout << "\nIngrese el texto en el archivo: ";
        cin >> textoArchivo;
        archivoOf << textoArchivo << endl;
        archivoOf.close();
        cout << "\nEl texto se ha añadido al archivo";
        system("pause");
    }
    else
        cout << "\nEl archivo especificado no existe, verifique el nombre";

}

void leerRegistro(string nombreArchivo) {
    string nombre;
    cout << "leyendo registro...";
    cout << "\nIngrese el nombre del archivo: ";
    cin >> nombre;
    nombre = nombre + ".txt";
    if (nombre == nombreArchivo) {
        cout << "\nSe encontro el siguiente texto en el archivo: \n";
        string texto;
        archivoIf.open(nombreArchivo.c_str(),ios::in);
        if (archivoIf.fail()) {
            cout << "No se puede abrir el archivo";
            exit(1);
        }
        while(!archivoIf.eof()) {
            getline(archivoIf,texto);
            cout << texto << endl;
        }
        archivoIf.close();
        system("pause");
    }
    else
        cout << "\nEl archivo especificado no existe, verifique el nombre";

}

void borrarContenido(string nombreArchivo) {
    string nombre;
    cout << "borrando contenido...";
    cout << "\nIngrese el nombre del archivo: ";
    cin >> nombre;
    nombre = nombre + ".txt";
    if (nombre == nombreArchivo) {
        archivoIf.open(nombreArchivo.c_str(),ios::out|ios::trunc);
        archivoIf.close();
        cout << "\nEl contenido ha sido borrado";
        system("pause");
    }
    else
        cout << "\nEl archivo especificado no existe, verifique el nombre";

}